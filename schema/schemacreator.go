package schema

import (
	"github.com/go-pg/pg/v10"
	"github.com/go-pg/pg/v10/orm"
)

type Userregloan struct {
	id 				uint
	pin       		int64
	phonenumber     int64
	birthday       	int64
	fullname   	 	string
	mothername    	string
	email    		string
}

func CreateSchema(db *pg.DB) error {
	models := []interface{}{
        &Userregloan{},
	}
	
	ops := &orm.CreateTableOptions{
		Temp: true,
		IfNotExists : true,
	}
	
	for _, model := range models {
        err := db.Model(model).CreateTable(ops)
        if err != nil {
            return err
        }
    }
    return nil
}