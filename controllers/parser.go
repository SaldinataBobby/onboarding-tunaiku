package controllers

import (
	"strconv"
	"fmt"
)

func parseBirthDatePin(pin string) string {
	pinRune := []rune(pin)
	bDay	:= ""
	mDay 	:= string(pinRune[8:10])
	yDay 	:= string(pinRune[10:12])
	x, _ := strconv.ParseInt(string(pinRune[6:8]), 10, 64)

	if x > 40 {
		date := x - 40
		bDay = strconv.FormatInt(date, 10)
		if len(strconv.FormatInt(date, 10)) == 1 {
			bDay = fmt.Sprintf("0" + strconv.FormatInt(date, 10))
		}
	}else{
		if len(strconv.FormatInt(x, 10)) == 1 {
			bDay = fmt.Sprintf("0" + strconv.FormatInt(x, 10))
		} else {
			bDay = strconv.FormatInt(x, 10)
		}
	}

	return bDay+"-"+mDay+"-"+yDay
}

func parseBirthDateInput(bi string) string {
	biRune 	:= []rune(bi)
	bDay	:= string(biRune[0:2])
	mDay	:= string(biRune[3:5])
	yDay	:= string(biRune[8:])

	return bDay+"-"+mDay+"-"+yDay
}

func parseSex(pin string) string {
	pinRune := []rune(pin)
	s		:= ""
	x, _ 	:= strconv.ParseInt(string(pinRune[6:8]), 10, 64)

	if x>40 {
		s = "Woman"
	}else{
		s = "Man"
	}

	return s
}
