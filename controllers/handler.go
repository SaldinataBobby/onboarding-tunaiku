package controllers

import (
	"net/http"
	"encoding/json"
	"fmt"

	"github.com/go-chi/render"

	model "regloan/models"
	repo "regloan/repository"
)

var res model.ResInsUpDel
var datLoan repo.DataLoan
var userData repo.UserData
var userdata *repo.UserData
var userloan *repo.UserLoan
var addruser *repo.AddrUser
var addrUser repo.AddrUser
var userLoan repo.UserLoan

func DoRegis(w http.ResponseWriter, r *http.Request) {
	msg		:= ""
	status	:= ""

	err := json.NewDecoder(r.Body).Decode(&datLoan)
	defer r.Body.Close()

	if err != nil {
		msg 	= "bad request"
		status 	= "process fail"
	}else{
		if DoValidationReg(datLoan.Fullname, datLoan.Pin, datLoan.Phonenumber, datLoan.Email, datLoan.Birthdate ) {
			exists := repo.ChkPinPhUniqu(datLoan.Pin, datLoan.Phonenumber)

			if !exists {
				userData := insertUserDataStructs(datLoan,parseSex(datLoan.Pin))
			 	addrUser := insertAddUserStructs(datLoan)
				userLoan := insertUserLoanStructs(datLoan)

				err := repo.InsertData(userData,addrUser,userLoan)
				if err != nil {
					msg 	= "data have not been inserted"
					status 	= "process failed"
				}else{
					msg 	= "data has been inserted"
					status 	= "process success"
				}
			}else{
				msg 	= "pin or phonenumber already registered"
				status 	= "process failed"
			}
		}else {
			msg 	= "validation failed"
			status 	= "process failed"
		}
	}

	res := insrtResStruct(msg, status)
	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, res)
}

func UserData(w http.ResponseWriter, r *http.Request){
	err := json.NewDecoder(r.Body).Decode(&datLoan)
	defer r.Body.Close()

	if err != nil {
		fmt.Println("what a format")
	}else{
		userdata = repo.GetUserData(datLoan.Pin)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, userdata)
}

func AddrUser(w http.ResponseWriter, r *http.Request){
	err := json.NewDecoder(r.Body).Decode(&datLoan)
	defer r.Body.Close()

	if err != nil {
		fmt.Println("what a format")
	}else{
		addruser = repo.GetAddrUser(datLoan.Pin)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, addruser)
}

func UserLoan(w http.ResponseWriter, r *http.Request){
	err := json.NewDecoder(r.Body).Decode(&datLoan)
	defer r.Body.Close()

	if err != nil {
		fmt.Println("what a format")
	}else{
		userloan = repo.GetUserLoan(datLoan.Pin)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, userloan)
}

func FullUserLoan(w http.ResponseWriter, r *http.Request) {
	err := json.NewDecoder(r.Body).Decode(&datLoan)
	defer r.Body.Close()
	cdl := &repo.CombineDataLoan{}

	if err != nil {
		fmt.Println("what a format")
	}else{
		userdata = repo.GetUserData(datLoan.Pin)
		addruser = repo.GetAddrUser(datLoan.Pin)
		userloan = repo.GetUserLoan(datLoan.Pin)

		cdl = &repo.CombineDataLoan{
			UserData : userdata,
			AddrUser : addruser,
			UserLoan : userloan,
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, cdl)
}

func DoDelete(w http.ResponseWriter, r *http.Request){
	err := json.NewDecoder(r.Body).Decode(&datLoan)
	defer r.Body.Close()

	if err != nil {
		fmt.Println("what a format")
	}else{
		err := repo.DeleteLoan(datLoan.Pin)
		if err != nil {
			panic(err)
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, res)
}

func UserDataUpdate(w http.ResponseWriter, r *http.Request){
	msg := ""
	st  := ""
	err := json.NewDecoder(r.Body).Decode(&userData)
	defer r.Body.Close()
	
	if err != nil {
		fmt.Println("what a format")
	}else{
		fmt.Println(DoValidationUpUserDat(userData.Fullname, userData.Phonenumber, userData.Email))
		if DoValidationUpUserDat(userData.Fullname, userData.Phonenumber, userData.Email){
			err = repo.UpdateUserData(userData)
			if err != nil {
				msg	= "problem update"
				st	= "Update success"
			}else{
				msg	= "No problem update"
				st	= "Update success"
			}
		}
	}

	res := insrtResStruct(msg, st)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, res)
}

func AddrUserUpdate(w http.ResponseWriter, r *http.Request){
	msg := ""
	st  := ""
	err := json.NewDecoder(r.Body).Decode(&addrUser)
	defer r.Body.Close()
	
	if err != nil {
		fmt.Println("what a format")
	}else{
		err = repo.UpdateAddrUser(addrUser)
			if err != nil {
				msg	= "problem update"
				st	= "Update success"
			}else{
				msg	= "No problem update"
				st	= "Update success"
			}
	}

	res := insrtResStruct(msg, st)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, res)
}

func UserLoanUpdate(w http.ResponseWriter, r *http.Request){
	msg := ""
	st  := ""
	err := json.NewDecoder(r.Body).Decode(&userLoan)
	defer r.Body.Close()
	
	if err != nil {
		fmt.Println("what a format")
	}else{
		err = repo.UpdateUserLoan(userLoan)
			if err != nil {
				msg	= "problem update"
				st	= "Update success"
			}else{
				msg	= "No problem update"
				st	= "Update success"
			}
	}

	res := insrtResStruct(msg, st)

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, res)
}

func insrtResStruct(msg string, st string) model.ResInsUpDel {
	return model.ResInsUpDel{
		Status	: st,
		Message : msg,
	}
}

func insertUserDataStructs(dataLoan repo.DataLoan, sex string) repo.UserData{
	return repo.UserData{
		Pin			: dataLoan.Pin,
		Phonenumber	: dataLoan.Phonenumber,
		Birthdate	: dataLoan.Birthdate,
		Fullname	: dataLoan.Fullname,
		Mothername	: dataLoan.Mothername,
		Sex 		: sex,
		Email		: dataLoan.Email,
	}
}

func insertAddUserStructs(dataLoan repo.DataLoan) repo.AddrUser {
	return repo.AddrUser{
		Pin 		: dataLoan.Pin,
		Province	: dataLoan.Province,
		City		: dataLoan.City,
		Fulladdress	: dataLoan.Fulladdress,
		Postcode	: dataLoan.Postcode,
	}
}

func insertUserLoanStructs(dataLoan repo.DataLoan) repo.UserLoan {
	return repo.UserLoan{
		Pin 		: dataLoan.Pin,
		Amountloan	: dataLoan.Amountloan,
		Period		: dataLoan.Period,
		Ipaddr		: dataLoan.Ipaddr,
	}
}