package controllers

import (
	"regexp"
)

var (
	emailRegex	= regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	nameRegex	= regexp.MustCompile("^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$")
	phoneRegex	= regexp.MustCompile("^\\+62[0-9]{9,16}$")
	pinRegex	= regexp.MustCompile("[0-9]+")
)

func DoValidationReg(fn string, pin string, ph string, email string, bd string) bool {
	result		:= false
	fnValid		:= isFullNameValid(fn)
	pinValid	:= isPINValid(pin)
	phValid		:= isPhoneValid(ph)
	emailValid 	:= isEmailValid(email)
	bdValid 	:= isBirthDateValid(pin, bd)

	if(fnValid && pinValid && phValid && emailValid && bdValid){
		result = true
	}
	return result
}

func DoValidationUpUserDat(fn string, ph string, email string) bool {
	result		:= false
	fnValid		:= isFullNameValid(fn)
	phValid		:= isPhoneValid(ph)
	emailValid 	:= isEmailValid(email)

	if(fnValid && phValid && emailValid){
		result = true
	}
	return result
}

func isPINValid(pin string) bool {
	if len(pin) > 16 {
		return false
	}
	return pinRegex.MatchString(pin)
}

func isPhoneValid(ph string) bool {
	if len(ph) > 16 {
		return false;
	}
	return phoneRegex.MatchString(ph)
}

func isEmailValid(email string) bool {
	if len(email) < 3 && len(email) > 254 {
		return false
	}
	return emailRegex.MatchString(email)
}

func isFullNameValid(fn string) bool {
	if len(fn) > 50 {
		return false
	}
	return nameRegex.MatchString(fn)
}

func isBirthDateValid(pin string, inp string) bool {
	pinBD := parseBirthDatePin(pin)
	inpBD := parseBirthDateInput(inp)

	if pinBD!=inpBD {
		return false
	}
	return true
}