package controllers

import (
	"net/http"
	"encoding/json"
	//"fmt"

	"github.com/go-chi/chi"
	"github.com/go-chi/render"
	"github.com/golang/gddo/httputil/header"
	logger "github.com/sirupsen/logrus"

	repo "regisloan/repository"
	model "regisloan/models"
)

var (
	jsonData []byte
)

func Routes() *chi.Mux{
	router := chi.NewRouter()

	router.Post("/", InsertData)
	router.Get("/", GetDatas)
	router.Get("/{pin}", GetData)
	router.Put("/{pin}", UpdateData)
	router.Delete("/{pin}", DeleteData)

	return router
}


func InsertData(w http.ResponseWriter, r *http.Request){

	contentType := r.Header.Get("Content-type")
	response := &model.ResInsUpDel{}

	if contentType == "" {
		msg := "empty content-type in header message"
		response = &model.ResInsUpDel{
			Status : "process fail",
			Message : msg,
		}
	} else {
		value, _ := header.ParseValueAndParams(r.Header, "Content-Type")
		
		if value != "application/json" {
			msg := "content-type is not application/json"
			response = &model.ResInsUpDel{
				Status : "process fail",
				Message : msg,
			}
		}else{
			data := repo.DataLoan{}
			err := json.NewDecoder(r.Body).Decode(&data)
			defer r.Body.Close()

			logger.WithFields(logger.Fields{
				"pin"		: reverse(data.Pin),
				"fullname"	: data.Fullname,
			  }).Info("message incoming ")

			if err != nil {
				msg := "bad request"
				response = &model.ResInsUpDel{
					Status : "process fail",
					Message : msg,
				}
			}else{
				pinValidResult:= PinValidation(data.Pin)
				phValidResult := PhoneNumberValidation(data.Phonenumber)

				msg 	:= ""
				status 	:= ""

				if(pinValidResult && phValidResult){
					exists,err := repo.ChkPIN(data.Pin)
					
					if err != nil {
						panic(err)
					}

					if !exists {
						err := repo.InsertData(data)

						if err != nil {
							msg 	= "data have not been inserted"
							status 	= "process failed"
						}else{
							msg 	= "data has been inserted"
							status 	= "process success"
						}
					}else{
						msg 	= "pin existed in database"
						status 	= "process failed"
					}
				
					response = &model.ResInsUpDel{
						Status	: status,
						Message : msg,
					}
				} else{
					msg 	= "validation problem"
					status 	= "process failed"

					response = &model.ResInsUpDel{
						Status	: status,
						Message : msg,
					}
				}
			}
		}
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-type", "application/json")
	render.JSON(w, r, response)
		
}

func GetDatas(w http.ResponseWriter, r *http.Request){
	
}

func GetData(w http.ResponseWriter, r *http.Request){
	
}

func UpdateData(w http.ResponseWriter, r *http.Request){
	
}

func DeleteData(w http.ResponseWriter, r *http.Request){
	
}