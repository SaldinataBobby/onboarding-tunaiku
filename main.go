package main

import (
	"fmt"
	"net/http"
	"time"
	"log"
	"os"
	"context"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
	"github.com/joho/godotenv"

	infra "regloan/infrastructure"
	ctrl "regloan/controllers"
)


var (
	dbHost string
	dbPort string
	dbUser string
	dbPassword string
	dbName string
)


func main(){

	// -----------------------------------------------------------------------------------------------------------------------
	// LOAD .ENV 
	// -----------------------------------------------------------------------------------------------------------------------
	err 	:= godotenv.Load()
	if err != nil {
		log.Fatal("Error loading .env file")
	}

	dbHost 		= os.Getenv("POSTGRES_HOST")
	dbPort 		= os.Getenv("POSTGRES_PORT")
	dbUser 		= os.Getenv("POSTGRES_USER")
	dbPassword 	= os.Getenv("POSTGRES_PASSWORD")
	dbName 		= os.Getenv("POSTGRES_DB")


	// -----------------------------------------------------------------------------------------------------------------------
	// INFRASTRUCTURE CONFIG
	// -----------------------------------------------------------------------------------------------------------------------
	db 		:= infra.InitDB(dbHost, dbPort, dbUser, dbPassword, dbName)
	
	ctx 	:= context.Background()
	if err 	:= db.Ping(ctx); err != nil {
		panic(err)
	}


	// -----------------------------------------------------------------------------------------------------------------------
	// ROUTE CONFIG
	// -----------------------------------------------------------------------------------------------------------------------
	r := chi.NewRouter()

	// CORS MIDDLEWARE :
	cors := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "PATCH", "OPTIONS"},
		AllowedHeaders: []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: true,
		MaxAge : 300,
	})

	r.Use(
		middleware.RequestID,
		middleware.RealIP,
		middleware.Logger,
		middleware.Recoverer,
		cors.Handler,
		middleware.Timeout(60 * time.Second),
	)

	r.Route("/api/v1", func(r chi.Router){
		r.Post("/regloan", ctrl.DoRegis) // insert registration

		r.Get("/userdata", ctrl.UserData) // get user data
		r.Get("/addruser", ctrl.AddrUser) // get addr user
		r.Get("/userloan", ctrl.UserLoan) // get user loan
		r.Get("/fulluserloan", ctrl.FullUserLoan) // get full user loan

		r.Delete("/deleteloan", ctrl.DoDelete) // delete loan

		r.Put("/userdata", ctrl.UserDataUpdate) // update userdata
	 	r.Put("/addruser", ctrl.AddrUserUpdate) // update adduser
		r.Put("/userloan", ctrl.UserLoanUpdate) // update userloan
	})


	// -----------------------------------------------------------------------------------------------------------------------
	// SERVE
	// -----------------------------------------------------------------------------------------------------------------------
	fmt.Println("Starting server on port : 3333")
	err = http.ListenAndServe(":3333", r)

	if err!=nil {
		fmt.Println("ListenAndServe : ", err)
	}
}
