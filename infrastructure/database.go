package infrastructure

import (
	"fmt"
	"github.com/go-pg/pg/v10"
)

var Db *pg.DB

func InitDB(dbHost string, dbPort string, dbUser string, dbPassword string, dbName string ) *pg.DB {
	Db = pg.Connect(&pg.Options{
		Addr	: fmt.Sprintf("%s:%s", dbHost, dbPort),
		User	: dbUser,
		Password: dbPassword,
		Database: dbName,
	})

	return Db
}