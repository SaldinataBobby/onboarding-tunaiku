package repository

import (
	//"fmt"

	//"github.com/davecgh/go-spew/spew"

	infra "regloan/infrastructure"
)

type DataLoan struct {
	Pin 			string	`json:"pin" validate:"required"`
	Phonenumber  	string	`json:"phonenumber" validate:"required"`
	Birthdate		string	`json:"birthdate" validate:"required"`
	Fullname		string	`json:"fullname" validate:"required"`
	Mothername		string	`json:"mothername" validate:"required"`
	Email			string	`json:"email" validate:"required"`
	Province		string	`json:"province" validate:"required"`
	City			string	`json:"city" validate:"required"`
	Fulladdress		string	`json:"fulladdress" validate:"required"`
	Postcode		string	`json:"postcode" validate:"required"`
	Amountloan		string	`json:"amountloan" validate:"required"`
	Period			string	`json:"period" validate:"required"`
	Ipaddr			string	`json:"ipaddr" validate:"required"`
}

type UserData struct {
	Pin 			string	`json:"pin" validate:"required"`
	Phonenumber  	string	`json:"phonenumber" validate:"required"`
	Birthdate		string	`json:"birthdate"`
	Fullname		string	`json:"fullname" validate:"required"`
	Mothername		string	`json:"mothername" validate:"required"`
	Sex 			string	`json:"sex"`
	Email 			string	`json:"email" validate:"required"`
}

type AddrUser struct {
	Pin 			string	`json:"pin"`
	Province		string	`json:"province"`
	City			string	`json:"city"`
	Fulladdress		string	`json:"fulladdress"`
	Postcode		string	`json:"postcode"`
}

type UserLoan struct {
	Pin 			string	`json:"pin"`
	Amountloan		string	`json:"amountloan"`
	Period			string	`json:"period"`
	Ipaddr			string	`json:"ipaddress"`
}

type CombineDataLoan struct {
	*UserData
	*AddrUser
	*UserLoan
}

func InsertData(ud UserData, au AddrUser, ul UserLoan) error {
	db := infra.Db

	_, err 	:= db.Model(&ud).Insert()
    if err != nil {
        panic(err)
	}

	_, err 	= db.Model(&au).Insert()
    if err != nil {
        panic(err)
	}

	_, err 	= db.Model(&ul).Insert()
    if err != nil {
        panic(err)
	}

	return nil
}

func GetUserData(pin string) *UserData {
	db := infra.Db
	ud := new(UserData)

	err := db.Model(ud).Where("pin = ?", pin).Select()
	if err != nil {
        panic(err)
	}

	return ud
}

func GetAddrUser(pin string) *AddrUser{
	db := infra.Db
	au := new(AddrUser)

	err := db.Model(au).Where("pin = ?", pin).Select()
	if err != nil {
        panic(err)
	}

	return au
}

func GetUserLoan(pin string) *UserLoan{
	db := infra.Db
	ul := new(UserLoan)

	err := db.Model(ul).Where("pin = ?", pin).Select()
	if err != nil {
        panic(err)
	}

	return ul
}

func DeleteLoan(pin string) error {
	db := infra.Db
	ud := new(UserData)
	au := new(AddrUser)
	ul := new(UserLoan)

	_, err := db.Model(ul).Where("pin = ?", pin).Delete()
	if err != nil {
        panic(err)
	}

	_, err = db.Model(au).Where("pin = ?", pin).Delete()
	if err != nil {
        panic(err)
	}

	_, err = db.Model(ud).Where("pin = ?", pin).Delete()
	if err != nil {
        panic(err)
	}

	return err
}

func UpdateUserData(ud UserData) error {
	db 			:= infra.Db
	exists,err 	:= chkPIN(ud.Pin)

	if err != nil {
		panic(err)
	}

	if exists{
		_, err := db.Model(&ud).Column("phonenumber", "fullname", "email", "mothername").Where("pin = ?", ud.Pin).Update()
		if err != nil {
			panic(err)
		}
	}
	return nil
}

func UpdateAddrUser(au AddrUser) error {
	db 			:= infra.Db

	_, err := db.Model(&au).Column("province", "city", "fulladdress", "postcode").Where("pin = ?", au.Pin).Update()
	if err != nil {
		panic(err)
	}
	
	return nil
}

func UpdateUserLoan(ul UserLoan) error {
	db := infra.Db
	
	_, err := db.Model(&ul).Column("amountloan", "period", "ipaddr").Where("pin = ?", ul.Pin).Update()
	if err != nil {
		panic(err)
	}
	
	return nil
}

func ChkPinPhUniqu(pin string, ph string) bool{
	cPin,_ 	:= chkPIN(pin)
	cPNum,_	:= chkPhNumber(ph)

	if (cPin && cPNum){
		return true
	}
	return false
}

func chkPIN(pin string) (bool, error) {
	db 		:= infra.Db
	userData:= UserData{}

	exists, err := db.Model(&userData).Where("pin = ?", pin).Exists()
	
	if err != nil {
		panic(err)
	}

	return exists, err
}

func chkPhNumber(ph string) (bool, error){
	db 		:= infra.Db
	userData:= UserData{}

	exists, err := db.Model(&userData).Where("phonenumber = ?", ph).Exists()
	
	if err != nil {
		panic(err)
	}

	return exists, err
}
