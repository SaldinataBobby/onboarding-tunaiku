module regloan

go 1.15

require (
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/go-chi/chi v1.5.1
	github.com/go-chi/cors v1.1.1
	github.com/go-chi/render v1.0.1
	github.com/go-pg/pg/v10 v10.7.5
	github.com/golang/gddo v0.0.0-20210115222349-20d68f94ee1f
	github.com/joho/godotenv v1.3.0
	github.com/sirupsen/logrus v1.7.0
	golang.org/x/sys v0.0.0-20210124154548-22da62e12c0c // indirect
)
